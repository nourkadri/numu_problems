import * as fs from 'fs';
import * as readline from 'node:readline';




//read data from file and convert it to array of rows
async function readLineByLine(path) {
    const listOfRows = [];
    const fileStream = fs.createReadStream(path);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    try {
        for await (const line of rl) {
            if (line.length != 0) {

                // remove "*" from numbers
                const newline = line.replace("*", '');

                

                //create array of words from newline
                const s = newline.split(' ');

                //remove empty string from array of words
                var filtered = s.filter(function (el) {
                    return el.length != 0;
                });

                //add filtered to listOfRows 
                listOfRows.push(filtered)
            }
        }
    } catch (error) {
        console.log(error)
    }
    //remove first element from array
    listOfRows.shift()

    return listOfRows;
}

export default readLineByLine;