export default function calculateDiff(listOfRows, elementIndex, indexOne, indexTwo) {
    let min = null;
    let result = [];
    let tempDiff = {};
    listOfRows.forEach((row) => {
        tempDiff[row[elementIndex]] = Math.abs(row[indexOne] - row[indexTwo]);
        if (min === null) {
            min = tempDiff[row[elementIndex]]
        }
        if (min > tempDiff[row[elementIndex]]) {
            min = tempDiff[row[elementIndex]];
            result = [row[elementIndex], min];
        }

    });
    return result;

}