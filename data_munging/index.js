import readLineByLine from "./assest/readData.js";
import calculateDiff from "./assest/calculateDiff.js";

const weather = readLineByLine('data/weather.dat')
const football = readLineByLine('data/football.dat')


weather.then(function (listOfRows) {
    const result = calculateDiff(listOfRows, 0, 1, 2)

    //print result
    console.log(`The minimum temperature spread is on day ${result[0]} and the temperature spread is ${result[1]}.`);
})



football.then(function (listOfRows) {
    const result = calculateDiff(listOfRows, 1, 6, 8)

    //print result
    console.log(`The smallest diff team is ${result[0]} and the difference is ${result[1]} goal.`);
})


