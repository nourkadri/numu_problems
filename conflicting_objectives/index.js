import readLineByLine from "./assest/readData.js";
import solution from "./assest/solution.js";
const wordlist = readLineByLine('data/wordlist.txt')

const start = Date.now();

wordlist.then(function (listOfRows) {
    console.log(solution(listOfRows,6))
    const end = Date.now();
    console.log(`Execution time: ${(end - start)/1000} S`);
})





