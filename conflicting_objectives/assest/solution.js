export default function (listOfRows,len) {
    let wordsWithSixChar = []
    let wordsWithLessThanSixChar = []
    let result = []
    wordsWithSixChar = listOfRows.filter(word => word.length === len);
    wordsWithLessThanSixChar = listOfRows.filter(word => word.length < len);
    wordsWithSixChar.forEach(word => {
        const subWord = split_word(word)
        subWord.forEach(sub => {
            if (wordsWithLessThanSixChar.includes(sub[0]) && wordsWithLessThanSixChar.includes(sub[1])) {
                result.push(`${sub[0]} + ${sub[1]} => ${word}`)
            }
        });
    });
    return result
}

// split words for example "nour" => [['n','our'],['no','ur']]
function split_word(word) {
    let sub = []
    let listSub = []
    for (let i = 0; i < 1; i++) {
        for (let j = 1; j < word.length - 1; j++) {
            sub.push(word.substr(i, j))
            sub.push(word.substr(j, word.length))
            listSub.push(sub)
            sub = []
        }
    }
    return listSub
}