import * as fs from 'fs';
import * as readline from 'node:readline';




//read data from file and convert it to array of rows
async function readLineByLine(path) {
    const listOfRows = [];
    const fileStream = fs.createReadStream(path);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    try {
        for await (const line of rl) {
            if (line.length != 0) {
                listOfRows.push(line.toLowerCase().trim())
            }
        }
    } catch (error) {
        console.log(error)
    }

    return listOfRows;
}

export default readLineByLine;